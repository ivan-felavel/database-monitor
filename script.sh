PATH=/usr/kerberos/sbin:/usr/kerberos/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin

service mysqld status|grep -v runn>/dev/null

if [ $? -eq 0 ]
then
	tail -10 /var/log/mysqld.log | mail -s DBDOWN root
	tail -10 /var/log/mysqld.log >/var/www/html/database-monitor/logfile.log
	echo "status1.push(\"0\");" >> /var/www/html/database-monitor/datos.js
	echo "times.push(\"`date +%d/%m/%Y_%H:%M`\");">>/var/www/html/database-monitor/datos.js
else
	echo "status1.push(\"1\");" >> /var/www/html/database-monitor/datos.js
	echo "times.push(\"`date +%d/%m/%Y_%H:%M`\");">>/var/www/html/database-monitor/datos.js
fi
